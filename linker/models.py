from django.db import models
from content import models as content

# Create your models here.

class EntryLink(models.Model):
    url = models.CharField(max_length=100)
    redirect_to = models.ForeignKey(content.Entry)
    
    def __str__(self):
        return(self.url)
        
class Custom(models.Model):
    url = models.CharField(max_length=100)
    redirect_to = models.CharField(max_length=100)
    
    def __str__(self):
        return(self.url)
