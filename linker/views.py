from django.shortcuts import render
from linker.models import *
from content import views as content
from django.http import HttpResponseRedirect
from django.http import Http404

# Create your views here.

def entry(request,url):
    resp=''
    try:
        e = EntryLink.objects.get(url=url)
        resp = content.entry(request,e.redirect_to.short_name)
    except Entry.DoesNotExist:
        resp = None
    return(resp)

def custom(request,url):
    resp=''
    try:
        c = Custom.objects.get(url=url)
        resp = HttpResponseRedirect(c.redirect_to)
    except Custom.DoesNotExist:
        resp = None
    return(resp)

def main(request,url):
    resp = content.custompage(request,url)
    if resp:
        return(resp)
    resp = entry(request,url)
    if resp:
        return(resp)
    resp = custom(request,url)
    if resp:
        return(resp)
    else:
        raise Http404
