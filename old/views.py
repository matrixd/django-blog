from django.shortcuts import render_to_response
from django.http import Http404, HttpResponse
from old.models import *
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
# Create your views here.

def entry(request, name):
    try:
        e = Entry.objects.get(id=name)
    except Entry.DoesNotExist:
        raise Http404
    return(render_to_response('old_entry.html', {'entry': e, 'tags': e.tags.all(),'old': True,'comments': e.comments}))

def tag(request, name):
    try:
        t = Tag.objects.get(short_name=name)
        entries = t.entry_set.filter(published=True).order_by('-id')
    except Tag.DoesNotExist:
        raise Http404
    return(render_to_response('tag.html', {'tag': t, 'entries': entries,'old': True}))

def tags(request):
    try:
        lst = []
        tags = Tag.objects.all()
        maxc = 0
        minc = 999
        for tag in tags:
            count = len(tag.entry_set.filter(published=True))
            if maxc<count:
                maxc=count
            if minc>count:
                minc=count
            lst.append((tag,count))
        out = []
        for  item in lst:
            size = 1
            maxfont = 25
            minfont = 16
            try:
                size = int(maxfont*(item[1]-minc)/(maxc-minc))
                if size==0:
                    size=minfont
            except ZeroDivisionError:
                size=maxfont
            out.append((item[0].name,item[0].get_absolute_url(),size))            
    except Tag.DoesNotExist:
        raise Http404
    return(render_to_response('tags.html', {'tags': out,'old': True}))

def mainpage(request):
    try:
        entries = Entry.objects.filter(published=True).order_by('-date')
        paginator = Paginator(entries, 10)
        page = request.GET.get('page')
        try:
            entries = paginator.page(page)
        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            entries = paginator.page(1)
        except EmptyPage:
            # If page is out of range (e.g. 9999), deliver last page of results.
            entries = paginator.page(paginator.num_pages)
    except Entry.DoesNotExist:
        raise Http404
    return(render_to_response('main.html', {'entries': entries,'range': range(1,paginator.num_pages+1),'old': True}))

def main(request):
    resp=None
    if len(request.META['QUERY_STRING'])==0:
        resp = mainpage(request)
    if request.META['QUERY_STRING'].count('=') == 1:
        arg,val = request.META['QUERY_STRING'].split('=')
        if arg == 'p':
            resp = entry(request,int(val))
        if arg == 'page':
            resp = mainpage(request)
    if resp:
        return(resp)
    else:
        raise Http404
