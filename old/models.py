from django.db import models
from django.core.urlresolvers import reverse

from pygments import highlight
from pygments.lexers import get_lexer_by_name
from pygments.formatters import HtmlFormatter


# Create your models here.

#categories
class Tag(models.Model):
    name = models.CharField(max_length=30)
    short_name = models.SlugField('Кртакое название(url)',max_length=30,unique=True)
    
    def get_absolute_url(self):
        return(reverse('old.views.tag', args=[self.short_name]))
        #return('fake')
    
    def __str__(self):
        return(self.name)

class Entry(models.Model):
    name =  models.CharField('Название',max_length=200)
    date = models.DateField('Дата публикации')
    content = models.TextField('Содержимое статьи',max_length=10000)
    short_content = models.TextField('Краткое содержимое статьи',max_length=500)
    tags = models.ManyToManyField(Tag,null=True,blank=True)
    published = models.BooleanField('Опубликовано?')
    comments = models.TextField(null=True,blank=True)
    
    def __str__(self):
        return(self.name)
    
    def get_absolute_url(self):
        return(reverse('old.views.entry', args=[self.id]))
        #return(reverse('old.views.entry', args=[str('fake')]))
        #return('/'+self.short_name)
        
    def save(self, *args, **kwargs):
    
        first_time=True
        while('[code=' in self.content):
            if first_time:
                self.content += '\n<link href="/static/css/highlight.css" rel="stylesheet">'
                first_time=False
            start = self.content.find('[code=')+6
            end = self.content[start:].find('[/code]')+start
            block = self.content[start:end]
            lang = block[:block.find(']')]
            if py in lang:
                lang = py3
            code = block[block.find(']')+1:]
            #lines = block.count('\n')
            
            lexer = get_lexer_by_name(lang.lower(), stripall=True)
            #formatter = HtmlFormatter(linenos=False, cssclass="source")
            formatter = HtmlFormatter(linenos=True, cssclass="source")
            res = highlight(code, lexer, formatter)
            #res = '<table class="table table-hover source"><tbody><tr><td>1</td><td>'+res[len('<div class="source">'):-6]
            #for k in range(2,lines+1):
            #    res = res.replace('\n','</pre></td></tr><tr><td>'+str(k)+'</td><td><pre>',1)
            #res += '</td></tr></tbody></table>'
                      
            self.content = self.content[:start-6]+'<div class="well well-sm">'+res+'</div>'+self.content[end+6+len(lang):]
            print(self.content)
        
        if '<!--more-->' in self.content:
            more = self.content.find('<!--more-->')
            self.short_content = self.content[:more]
            self.content = self.content.replace('<!--more-->','')
        
        self.content = self.content.replace('http://matrixd.ru/wp-content/uploads','/media/old') 
        
        while( '[caption' in self.content):
            start=self.content.find('[caption')+len('[caption')
            end=self.content.find('[/caption]')+len('[/caption]')
            new=self.content[start:end]
            caption=new[new.find('caption="')+len('caption="'):new.find('"]')]
            link=new[new.find('href="')+len('href="'):new.find('">')]
            tmp=new[new.find('src="')+len('src="'):]
            src=tmp[:tmp.find('"')]
            tmp=new[new.find('width="')+len('width="'):]
            width=tmp[:tmp.find('"')]
            tmp=new[new.find('height="')+len('height="'):]
            height=tmp[:tmp.find('"')]
            new='<a class="thumbnail" href="%s"><img src="%s" width="%s" height="%s"></img>' % (link,src,width,height)
            new+='<span class="caption">%s</span></a>' % caption

            self.content=self.content[:start-len('[caption')]+new+self.content[end:]
        
        super(Entry, self).save(*args, **kwargs)
