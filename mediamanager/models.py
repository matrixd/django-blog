from django.db import models
from content.models import Entry

# Create your models here.

#settings list
#   thumb_size = (300,300)
#   

class Settings(models.Model):
    name = models.CharField(max_length=100)
    val = models.CharField(max_length=100)

class Picture(models.Model):
    
    from django.core.files.storage import FileSystemStorage
    
    
    pic = models.ImageField(upload_to='images')
    entries = models.ManyToManyField(Entry,null=True,blank=True)
    width = models.PositiveSmallIntegerField(null=True)
    height = models.PositiveSmallIntegerField(null=True)
        
    def delete(self, *args, **kwargs):
        # You have to prepare what you need before delete the model
        storage, path = self.pic.storage, self.pic.name
        # Delete the model before the file
        super(Picture, self).delete(*args, **kwargs)
        # Delete the file after the model
        storage.delete(path)
        ext = path.split('.')[-1]
        storage.delete(path[:-(len(ext)+1)]+'_thumb.'+ext)
        storage.delete(path[:-(len(ext)+1)]+'_small.'+ext)
        
    def save(self, *args, **kwargs):
        #changing the directory
        try:
            pic = Picture.objects.get(pk=self.pk)
        except Picture.DoesNotExist:
            pic = None
            
        if pic != None:
            storage, path = pic.pic.storage, pic.pic.path
            storage.delete(path)
            ext = path.split('.')[-1]
            l = len(path)-len(ext)-1
            link = path[:l]+'_thumb.'+ext
            storage.delete(link)
            link = path[:l]+'_small.'+ext
            storage.delete(link)
        
        super(Picture, self).save(*args, **kwargs)
        
        from PIL import Image
        
        thumb = Image.open(self.pic)
        
        name = self.pic.path.split('.')[-2]
        
        size = (int(Settings.objects.get(name__exact="thumb_size").val.split('x')[0]),int(Settings.objects.get(name__exact="thumb_size").val.split('x')[1]))
        
        thumb.thumbnail(size,Image.ANTIALIAS)
        self.width,self.height = thumb.size
        thumb.save(name + '_thumb.jpg', "JPEG")
        thumb.thumbnail((100,100),Image.ANTIALIAS)
        thumb.save(name + '_small.jpg', "JPEG")
            
        super(Picture, self).save(*args, **kwargs)
            
            
    def __str__(self):
        return(self.pic.name.split('/')[-1])
    
    def thumb(self):
        ext = self.pic.name.split('.')[-1]
        l = len(ext)+1
        link = self.pic.url[:-l]+'_thumb.'+ext
        return(link)
        
    def small(self):
        ext = self.pic.name.split('.')[-1]
        l = len(ext)+1
        link = self.pic.url[:-l]+'_small.'+ext
        return(link)
        
    def code(self):
        return('<a class="thumbnail" href="'+self.pic.url+'"><img width="'+str(self.width)+'px" height="'+str(self.height)
                    +'px"src="'+self.thumb()+'"></img></a>')
    
    class Meta:
        verbose_name = 'Изображение'
        verbose_name_plural = 'Изображения'
