from django.contrib import admin
from mediamanager.models import *

# Register your models here.


class PicAdmin(admin.ModelAdmin):
    #global DispReleated
    
    exclude = ('entries','width','height')
    
    readonly_fields=('img','code','DispReleated')
    
    def DispReleated(self, instance):
        html = '<table><tr><td>Article</td><td>Date published</td></tr>'
        for entry in instance.entries.all():
            html += '<tr><td><a href="/admin/content/entry/%d/">%s</a></td><td>%s</td></tr>' % (entry.id,entry, entry.date)
            html += '</table>'
        return(html)
    DispReleated.allow_tags = True
    
    def img(self, instance):
    	link = instance.thumb()
    	return('<a href="%s"><img src="%s"></img></a>' % (instance.pic.url, link))
    img.allow_tags = True
    
    def code(self,instance):
        return("""<input size='150px' type='text' value='%s'></input>""" % instance.code())
    code.allow_tags = True
    
    def name(self, instance):
    	return(str(instance))
    
    list_display = ('name', 'img','DispReleated')
    
class SettingsAdmin(admin.ModelAdmin):
	list_display = ('name', 'val')

admin.site.register(Picture, PicAdmin)
admin.site.register(Settings, SettingsAdmin)
