from django.contrib import admin
from content.models import *
from mediamanager import models as media

class AddImage(admin.TabularInline):
    model = media.Picture.entries.through
    extra = 1
    
    def img(self, instance):
        return("""<img onclick='prompt("Cpy&paste","%s");' src='%s'></img>""" % (instance.picture.code().replace('"','\\"'),instance.picture.small()))

    img.allow_tags = True
    readonly_fields=('img',)

class EntryAdmin(admin.ModelAdmin):
    
    inlines = [AddImage]
    
    list_display = ('name', 'short_name', 'date', 'published')

class TagAdmin(admin.ModelAdmin):
    def quantity(self, instance):
        return(len(instance.entry_set.filter(published=True)))
    list_display = ('name', 'short_name','quantity')
     
admin.site.register(Entry, EntryAdmin)
admin.site.register(Tag, TagAdmin)
admin.site.register(Page)
