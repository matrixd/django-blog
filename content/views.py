from django.shortcuts import render_to_response
from django.http import Http404
from content.models import *
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
# Create your views here.

def entry(request, name):
    try:
        e = Entry.objects.get(short_name=name)
    except Entry.DoesNotExist:
        raise Http404
    return(render_to_response('entry.html', {'entry': e, 'tags': e.tags.all()}))

def tag(request, name):
    try:
        t = Tag.objects.get(short_name=name)
        entries = t.entry_set.filter(published=True).order_by('-date')
        paginator = Paginator(entries, 10)
        page = request.GET.get('page')
        try:
            entries = paginator.page(page)
        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            entries = paginator.page(1)
        except EmptyPage:
            # If page is out of range (e.g. 9999), deliver last page of results.
            entries = paginator.page(paginator.num_pages)
    except Tag.DoesNotExist:
        raise Http404
    return(render_to_response('tag.html', {'tag': t, 'entries': entries,'range': range(1,paginator.num_pages+1)}))

def tags(request):
    try:
        lst = []
        tags = Tag.objects.all()
        maxc = 0
        minc = 999
        for tag in tags:
            count = len(tag.entry_set.filter(published=True))
            if maxc<count:
                maxc=count
            if minc>count:
                minc=count
            lst.append((tag,count))
        out = []
        for  item in lst:
            size = 1
            maxfont = 25
            minfont = 16
            try:
                size = int(maxfont*(item[1]-minc)/(maxc-minc))
                if size==0:
                    size=minfont
            except ZeroDivisionError:
                size=maxfont
            out.append((item[0].name,item[0].get_absolute_url(),size))            
    except Tag.DoesNotExist:
        raise Http404
    return(render_to_response('tags.html', {'tags': out}))

def mainpage(request):
    try:
        entries = Entry.objects.filter(published=True).order_by('-date')
        paginator = Paginator(entries, 10)
        page = request.GET.get('page')
        try:
            entries = paginator.page(page)
        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            entries = paginator.page(1)
        except EmptyPage:
            # If page is out of range (e.g. 9999), deliver last page of results.
            entries = paginator.page(paginator.num_pages)
    except Entry.DoesNotExist:
        raise Http404
    nums = ''
    if paginator.num_pages>1:
        nums = range(1,paginator.num_pages+1)
    return(render_to_response('main.html', {'entries': entries,'range': nums}))

def custompage(request,url):
    resp=''
    try:
        p = Page.objects.get(url=url)
        resp = render_to_response('page.html', {'content': p.content})
    except Page.DoesNotExist:
        resp = None
    return(resp)
