from django.conf.urls import patterns, include, url
from django.conf import settings
from content import views as content
from linker import views as linker

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'blog.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^entries/(\w+)/$', content.entry),
    url(r'^tag/(\w+)/$', content.tag),
    url(r'^tags/$', content.tags),
    url(r'^$', content.mainpage),
    url(r'^(\w+)/$', linker.main),
)

if settings.DEBUG:
    from django.conf.urls.static import static
    urlpatterns += patterns('',
        (r'^media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': '/home/matrixd/projects/blog/media/'}),
    ) + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
