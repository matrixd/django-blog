from django.conf.urls import patterns, include, url
from django.conf import settings
from old import views as old

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'blog.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^tag/(\w+)/$', old.tag),
    url(r'^tags/$', old.tags),
    #url(r'^$', old.mainpage),
    url(r'^entry/(\d+)/$', old.entry),
    #url(r'^(\w+)/$', linker.main),
    url(r'^$', old.main),
)

if settings.DEBUG:
    from django.conf.urls.static import static
    urlpatterns += patterns('',
        (r'^media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': '/home/matrixd/projects/blog/media/'}),
    ) + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
